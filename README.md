# UFPAlign 1.0: Forced Phonetic Aligner using HTK

:warning: Disclaimer:
this was the very first forced phonetic alignment released by the FalaBrasil
group. It is outdated, therefore no support will ever be given any longer to
this aligner. Most recent solutions have been using Kaldi instead of HTK.


## Requirements

After registering to [HTK website](http://htk.eng.cam.ac.uk/download.shtml)
and downloading the latest stable release (3.4.1 as of Nov 2020), extract and
and install it from source:

```bash
$ tar xvvf HTK-3.4.1.tar.gz
$ cd htk/
$ ./configure --disable-hlmtools --disable-hslab
$ make all -j 6
$ sudo make install
```

:warning: if you're using Arch Linux, install packages from `multilib-devel` 
group as well.

If everything goes as planned, `HVite` tool should now be reachable via `PATH`
env var right away:

```text
$ HVite | head

USAGE: HVite [options] VocabFile HMMList DataFiles...

 Option                                       Default

 -a      align from label files               off
 -b s    def s as utterance boundary word     none
 -c f    tied mixture pruning threshold       10.0
 -d s    dir to find hmm definitions          current
 -e      save direct audio rec output         off
```

[Mono](https://www.mono-project.com/) C# / .NET "interpreter" for Linux is
also required, as well as [Praat](https://www.fon.hum.uva.nl/praat/), since
this is a Praat's plugin:

```bash
$ sudo pacman -S mono
$ yay -S praat  # available on Arch's AUR, dunno for other distros
```

Python 3 is also expected to be available. The demo to be shown later on was
recorded on version 3.8.6.


## Installation

The installation is as simple as copying the entire plugin dir to Praat's
workspace folder at the user's home directory:

```bash
$ cp -r plugin_ufpalign-1.0/ $HOME/.praat-dir/
```


## Usage

Open Praat. Then, under the `New` menu, the last item should be the UFPAlign
plugin. An IDE will then ask you to load a `.wav` audio file and its text
transcription `.txt`. Do it, and the TextGrid shall appear on the screen
seconds later.

![UFPAlign 1.0 demo](doc/demo.gif)


### Command line

```bash
$ bash align.sh example/LapsBM_0098.wav example/LapsBM_0098.txt dict.dict /tmp/out.TextGrid
```

If you don't have a phonetic dictionary in hand, edit the script to stop
receiving it as an argument and create it on the fly based on a word list 
using the `lapsg2p` C# script interpreted by `mono` on Linux.


## Citation

If you use either this entire tool or some of its resources within this repo,
please cite our
[PROPOR 2016 paper](https://link.springer.com/chapter/10.1007/978-3-319-41552-9_38)
as one of the following:

> Souza G., Neto N. (2016)
> An Automatic Phonetic Aligner for Brazilian Portuguese with a Praat Interface.
> In: Silva J., Ribeiro R., Quaresma P., Adami A., Branco A. (eds)
> Computational Processing of the Portuguese Language. PROPOR 2016.
> Lecture Notes in Computer Science, vol 9727. Springer, Cham.
> https://doi.org/10.1007/978-3-319-41552-9_38

```bibtex
@inproceedings{Souza16,
    author     = {Souza, Gleidson and Neto, Nelson},
    editor     = {Silva, Jo{\~a}o and Ribeiro, Ricardo and Quaresma, Paulo and Adami, Andr{\'e} and Branco, Ant{\'o}nio},
    title      = {An Automatic Phonetic Aligner for Brazilian Portuguese with a Praat Interface},
    booktitle  = {Computational Processing of the Portuguese Language},
    year       = {2016},
    publisher  = {Springer International Publishing},
    address    = {Cham},
    pages      = {374--384},
    isbn       = {978-3-319-41552-9}
}
```

## NOTE
For a faster processing a syll dict (_dicionário de separação silábica_) should
be created beforehand and passed as arg to the script. Then `createAux.py` and
`addSyllables.py` scripts should stop reading files from and writing files to
disk and communicate with `convert.py` by passing python objects while still in
RAM. This is easy to do in a half a day but I'm not on the mood -Cassio.


[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2016)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Iago Souza - ...    
