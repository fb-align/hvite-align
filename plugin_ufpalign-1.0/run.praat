
# Iniciando o label:
label SCREEN

# Design da janela Demo   
	demo Erase all
	demo Select inner viewport... 0 100 0 100
	demo Axes... 0 100 0 100

	# Título 
	demo Colour... 0
	demo Text special... 50 centre 95 half Times 20 0 UFPAlign
   
	demo Colour... 0
	demo Text special... 20 centre 85  half Times 15 0 Arquivo de áudio (WAV):
	demo Colour... 0
	demo Text special... 20 centre 75 half Times 15 0 Transcrição do áudio (TXT): 
	#demo Colour... 0
	#demo Text special... 20 centre 65 half Times 15 0 Salvar em:

	# Botão para procurar arquivo .wav
	demo Black
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 83 88 2
	demo Colour... 0
	demo Text special... 40 centre 85.5 half Times 15 0 Procurar...

	# Futuro botão para reconhecer arquivo .wav
	#demo Blue
	#demo Line width... 2
	#demo Draw rounded rectangle... 55 65 83 88 2
	#demo Colour... {0.8,0.1,0.2}
	#demo Text special... 60 centre 85.5 half Times 15 0 Reconhecer 

	# Botão para procurar arquivo .txt
	demo Black
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 73 78 2
	demo Colour... 0
	demo Text special...  40 centre 75.5 half Times 15 0 Procurar...
	
	# Botão para iniciar o alinhamento
	demo Black
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 63 68 2
	demo Colour... 0
	demo Text special... 40 centre 65.5 half Times 15 0 Alinhar
	
	# Botão Sair
	#demo Black
	#demo Line width... 2
	#demo Draw rounded rectangle... 85 95 5 10 2
	#demo Colour... 0
	#demo Text special... 90 centre 7.5 half Helvetica 10 0 SAIR

	# Esperando entrada do usuário
	while demoWaitForInput ()

			# If para verificar o clic no botão "procurar" para o arquivo wav
			# A variável wavFileName salva o caminho do arquivo de áudio
			if demoClickedIn (35, 45, 83, 88)
				wavFileName$ = chooseReadFile$ ("Escolha o arquivo de áudio:")
				wavFileName$ > temp/wav_list
				#wavFileName$ > temp/mfc_list
				#wavFileName$ > temp/lab_list
				goto SCREEN

			# If para verificar o clic no botão "procurar" para o arquivo txt
			# A variável txtFileName salva o caminho do arquivo de transcrição do áudio
			elsif demoClickedIn (35, 45, 73, 78)
				txtFileName$ = chooseReadFile$ ("Escolha a transcrição do áudio:")
				txtFileName$ > temp/txt_list
				system echo "$(cat $(cat temp/txt_list) | sed 's/ /\n/g')" > $(cat temp/txt_list | sed 's/\.txt/\.lab/g') 
				goto SCREEN
			
			# If para verificar o clic no botão "reconhecer"
			#elsif demoClickedIn(55, 65, 83, 88)
			#	demo Text special... 70 centre 80.5 half Times 15 0 RECONHECER!!!

			# If para verificar o clic no botão "alinhar"
			elsif demoClickedIn (35, 45, 63, 68)
				demo Paint rounded rectangle... red 35 45 63 68 2
				# Rodando createWordList.sh
				system util/createWordList.sh
				# Criando o dicionário usando o g2p
				system mono g2p/lapsg2p.exe -w temp/word_list -d temp/dictionary.temp
				# Adicionando sil no dicionário
				system echo '<s> sil\n</s> sil\n<unk> sil' > temp/dictionary
				system cat temp/dictionary.temp >> temp/dictionary 
				system rm -f temp/dictionary.temp
				system echo "sil sil" >> temp/dictionary
				
				# Rodando o HVite para gerar o arquivo alinhado
				system HVite -A -t 1 -b sil -a -m -C confs/comp.cfg -H model/hmmdefs -t 250.0 temp/dictionary util/phones.list $(cat temp/wav_list)
				system python util/convert.py -r $(cat temp/wav_list | sed 's/\.wav/\.rec/g') -t $(cat temp/wav_list  | sed 's/\.wav/\.TextGrid/g') 
				system cat temp/wav_list | sed 's/\.wav/\.TextGrid/g' > temp/tg_list
				system cat temp/wav_list | awk -F '/' '{print $NF}' | sed 's/\.wav//g' > temp/prefix
				#goto SCREEN2
			else
				goto SCREEN
			endif


label SCREEN2
	demo Erase all
	demo Select inner viewport... 0 100 0 100
	demo Axes... 0 100 0 100

	# Título 
	demo Colour... 0
	demo Text special... 50 centre 95 half Times 20 0 UFPAlign
	
	demo Colour... 0
	demo Text special... 50 centre 85  half Times 15 0 Seu arquivo foi alinhado com sucesso!
	demo Colour... 0
	demo Text special... 20 centre 75 half Times 15 0 Deseja abrir o arquivo alinhado?
	
	# Botão para procurar arquivo .txt
	demo Red
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 73 78 2
	demo Colour... {0.8,0.1,0.2}
	demo Text special...  40 centre 75.5 half Times 15 0 Sim

	demo Red
	demo Line width... 2
	demo Draw rounded rectangle...  48 58 73 78 2
	demo Colour... {0.8,0.1,0.2}
	demo Text special... 53 centre 75.5 half Times 15 0 Não

	while demoWaitForInput ()

		# If para verificar o clic no botão "procurar" para o arquivo wav
		# A variável wavFileName salva o caminho do arquivo de áudio
		if demoClickedIn (35, 45, 73, 78)
				wavFileName$ < temp/wav_list
				tgFileName$ < temp/tg_list
				prefix$ < temp/prefix
				Read from file... 'tgFileName$'
				nametg$ = selected$("TextGrid")
				Read from file... 'wavFileName$'
				namesound$ = selected$("Sound")
				select Sound 'namesound$'
				plus TextGrid 'nametg$'
				Edit
				goto SCREEN
		else
				goto SCREEN2

label END

