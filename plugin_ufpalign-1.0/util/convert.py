#!/usr/bin/env python
import os

import string
import argparse
import re
from os.path import basename
from os.path import join
from addSyllables import addSyllables
from createAux import createAux


def convert(fileName, output_dir):
    lat = re.sub(r'.rec', r'.lat', fileName)
    word_graf = createAux(fileName, lat)
    addSyllables(lat, lat+'.tmp')
    os.system("cat " + lat + " | uniq > " + lat + ".tmp")
    os.system("mv " + lat + ".tmp " + lat)
    f = open(lat)

    l = '.'
    ti = None
    prefix = None
    label = None
    label_syl = None
    cont = 1
    stop = 0
    stop_syl = 0
    start = 0
    label_list = []
    factor =  1 #10**-7
    out_f = None
    out_str = ''
    sec_out_str = ''
    third_out_str = ''
    interval = 1
    sec_interval = 1
    third_interval = 1
    write = True
    aux_label = ''
    wholeword = None
    aux_whole = ''
    setence = ''
    begin = '0.0'
    verify = 0

    prefix = output_dir
    aux_word = ''
    out_f = open(prefix, 'w')
    while write:
        l = f.readline()
        if len(l.split()) == 3:
            if l.split()[0] != "!NULL 0 0":
                wholeword, stop, etc = l.split()
                if sec_interval > 1:
                    sec_out_str += ('\t\t\txmax = %s\n' % stop)
                    sec_out_str += ('\t\t\ttext = "%s"\n' % aux_whole)
                sec_out_str += ('\t\tintervals [%d]\n' % sec_interval)
                sec_out_str += ('\t\t\txmin = %s\n' % stop)
                lab = re.sub(r'\\(?P<a>\d{3})\\(?P<b>\d{3})', lambda wholeword:
                            bytearray(map(int, (wholeword.group('a'),
                        wholeword.group('b')), [8]*2)).decode('utf-8'), wholeword)
                aux_whole = lab
                if lab != 'sil':
                    setence = setence + lab + ' '
                sec_interval += 1


        if len(l.split()) == 2:
            label, start = l.split()
            if label != 'sp':
                    
                if interval > 1:
                    if verify == 0:
                        begin = start
                        verify += 1
                    out_str += ('\t\t\txmax = %s\n' % start)
                    out_str += ('\t\t\ttext = "%s"\n' % aux_label)
                out_str += ('\t\tintervals [%d]\n' % interval)
                out_str += ('\t\t\txmin = %s\n' % start)
                interval += 1
                aux_label = label

        if len(l.split()) == 4:
            label_syl, stop_syl, a, b = l.split()
            third_out_str += ('\t\tintervals [%d]\n' % third_interval)    
            third_out_str += ('\t\t\txmin = %s\n' % begin)
            third_out_str += ('\t\t\txmax = %s\n' % stop_syl)
            begin = stop_syl
            third_out_str += ('\t\t\ttext= "%s"\n' % label_syl)
            third_interval += 1        

        if l == '':
            out_str += ('\t\t\txmax = %s\n' % start)
            out_str += ('\t\t\ttext = "%s"' % label)
            sec_out_str += ('\t\t\txmax = %s\n' % start)
            sec_out_str += ('\t\t\ttext = "%s"' % aux_whole)
            out_f.seek(0,0)
            out_f.write('File type = "ooTextFile"\nObject class = "TextGrid"\nxmin = 0\nxmax = %s\ntiers? <exists>\n' % start)
            out_f.write('size = 5\n' + 'item []:\n' + '\titem[1]:\n' + '\t\tclass = "IntervalTier"\n')
            out_f.write('\t\tname = "%s-fonemas"\n' % prefix)
            out_f.write('\t\txmin = 0\n\t\txmax = %s\n' % start)
            out_f.write('\t\tintervals: size = %d\n' % (interval - 1))
            out_f.write(out_str)
            out_f.write('\n\titem[2]:\n' + '\t\tclass = "IntervalTier"\n')
            out_f.write('\t\tname = "%s-silabas"\n' % prefix)
            out_f.write('\t\txmin = 0\n\t\txmax = %s\n' % start)
            out_f.write('\t\tintervals: size = %d\n' % (third_interval -1))
            out_f.write(third_out_str)
            out_f.write('\n\titem[3]:\n' + '\t\tclass = "IntervalTier"\n')
            out_f.write('\t\tname = "%s-palavras"\n' % prefix)
            out_f.write('\t\txmin = 0\n\t\txmax = %s\n' % start)
            out_f.write('\t\tintervals: size = %d\n' % (sec_interval -1))
            # out_f.write(sec_out_str.encode('utf-8'))  # cassota
            out_f.write(sec_out_str)
            out_f.write('\n\titem[4]:\n' + '\t\tclass = "IntervalTier"\n')
            out_f.write('\t\tname = "%s-frase-grafema"\n' % prefix)
            out_f.write('\t\txmin = 0\n\t\txmax = %s\n' % start)
            out_f.write('\t\tintervals: size = 1\n')
            out_f.write('\t\tintervals[1]\n')
            out_f.write('\t\t\txmin = 0\n')
            out_f.write('\t\t\txmax = %s\n' % start)
            out_f.write('\t\t\ttext = "%s"\n' % re.sub('-', '', str(word_graf)))
            out_f.write('\n\titem[5]:\n' + '\t\tclass = "IntervalTier"\n')
            out_f.write('\t\tname = "%s-frase"\n' % prefix)
            out_f.write('\t\txmin = 0\n\t\txmax = %s\n' % start)
            out_f.write('\t\tintervals: size = 1\n')
            out_f.write('\t\tintervals[1]\n')
            out_f.write('\t\t\txmin = 0\n')
            out_f.write('\t\t\txmax = %s\n' % start)
            out_f.write('\t\t\ttext = "%s"\n' % setence)

            out_f.close()
            break;
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument( '-r', '--rec-file', required=True,
            help='Rec File to convert')
    parser.add_argument( '-t', '--text-grid', required=True,
            help='TextGrid File')
    args = parser.parse_args()
    convert(args.rec_file, args.text_grid)
