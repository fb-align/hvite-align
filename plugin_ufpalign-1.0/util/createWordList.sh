#!/bin/bash

if [ -a temp/word_list ]; then
	rm -f temp/word_list
fi

cat `cat temp/txt_list` | sed 's/ /\n/g' >> temp/word_list
cat temp/word_list | sort | uniq > temp/word_list.temp
mv temp/word_list.temp  temp/word_list

