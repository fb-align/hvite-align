#!/usr/bin/env python
import os
import string
import argparse
import re
import logging
from subprocess import Popen, PIPE
from os.path import basename
from os.path import join

def addSyllables(fileName, output_file):

    syllables = ''
    phones = ''
    write = True
    end = 0
    aux = ''
    endphone = 0
    out_f2 = ''
    stop = ''
    a = 0
    out = ''
    word_graf = ''

    f = open(fileName)
    out_f = open(output_file, 'w')    

    while write:
        l = f.readline()
        if len(l.split()) == 1:
            syllables = re.sub('\n', '', l).split('-') 
            #out_f.write(syllables[0]+'\n')
            if len(syllables) >= 1:
                for x in range (0, len(syllables)):
                    while phones != syllables[x]:
                        l = f.readline()
                        if endphone == 1:
                            out_f.write(aux + " " + str(l.split()[1]) + " 0 0\n" )
                            endphone = 0
                        phones += l.split()[0]
                        out_f.write(l)
                    if phones == syllables[x]:
                        aux = syllables[x]
                        phones = ''
                        x += 1
                        endphone = 1
        if l == '':
            out_f.write(aux + " " + stop + " 0 0\n" )
            break
    
        if len(l.split()) > 1:
            stop = str(l.split()[1])
            if len(l.split()) == 2 and re.search('sil', str(l)):
                a += 1
                if a == 1:
                    out_f.write(l)
                else:
                    out = str(l)
            if len(l.split())== 3:
                out_f2 += l
        
    out_f.write(out)
    out_f.write(out_f2)
    f.close()
    out_f.close()
    
    os.remove(fileName)
    os.rename(output_file, fileName)
    return word_graf

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument( '-l', '--lat-file', required=True,
            help='Lattice File to convert')
    parser.add_argument( '-o', '--output-file', required=True,
            help='Output File')
    args = parser.parse_args()
    addSyllables(args.lat_file, args.output_file)
