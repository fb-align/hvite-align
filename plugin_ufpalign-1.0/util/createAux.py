#!/usr/bin/env python
import re
import argparse
from subprocess import Popen, check_output

def createAux(recFile, auxFile):
    write = True
    r = open(recFile)
    aux = open(auxFile, 'w')
    factor = 10**-7
    word_graf = ''

    while write:
        rec = r.readline()
        if rec == '':
            r.close()
            aux.close()
            break
        if len(rec.split()) == 5:
            begin, end, phone, score, word = rec.split()
            word = re.sub(r'\\(?P<a>\d{3})\\(?P<b>\d{3})', lambda word:           
                    bytearray(map(int, (word.group('a'),
                    word.group('b')), [8]*2)).decode('utf-8'), word)
            begin = float(begin) * factor
            end = float(end) * factor
            aux.write(word + "\t" + str(begin) + "\t0\n")
            if word != 'sil':
                syllables = check_output(["mono", "lapsseparador/lapsseparador.exe", "-g", word])
                syllables = syllables.decode("utf-8").split('\t', 1)[1]
                syllables = re.sub("'", "", str(syllables))
                word_graf = re.sub('\n', '', (word_graf + str(syllables) + " "))
                aux.write(syllables)
            else:
                aux.write('sil\n')
        if len(rec.split()) == 4:
            begin, end, phone, score = rec.split()
            begin = float(begin) * factor
            end = float(end) * factor
        aux.write(phone + "\t" + str(begin) + "\n")
    return word_graf
                    


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument( '-r', '--rec-file', required=True,
            help='Rec File to convert')
    parser.add_argument( '-a', '--aux-file', required=True,
            help='Auxiliar Output File')
    args = parser.parse_args()
    word_graf = createAux(args.rec_file, args.aux_file)
