#!/usr/bin/env bash
#
# based on plugin*/run.praat#L62-L98
#
# author: nov 2020
# cassio batista - https://cassota.gitlab.io

plugin_dir="plugin_ufpalign-1.0/"

if test $# -ne 4 ; then
    echo "usage: $0 <wav-file> <txt-file> <dict-file> <textgrid-file>"
    echo "  <wav-file> is the input audio file in MS WAVE format"
    echo "  <txt-file> is the text trancription input file"
    echo "  <dict-file> is the phonetic dictionary input file"
    echo "  <textgrid-file> is the TextGrid output file"
    exit 1
elif [[ $1 != *.wav ]] ; then
    echo "[$0] error: first arg should have '.wav' extension: $1"
    exit 1
elif [[ $2 != *.txt ]] ; then
    echo "[$0] error: second arg should have '.txt' extension: $2"
    exit 1
elif [[ $3 != *.dict ]] ; then
    echo "[$0] error: third arg should have '.dict' extension: $3"
    exit 1
elif [[ $4 != *.TextGrid ]] ; then
    echo "[$0] error: third arg should have '.TextGrid' extension: $3"
    exit 1
elif [ ! -f "$1" ] ; then
    echo "[$0] error: audio file does not exist: $1"
    exit 1
elif [ ! -f "$2" ] ; then
    echo "[$0] transription file does not exist: $2"
    exit 1
elif [ ! -f "$3" ] ; then
    echo "[$0] phonetic dictionary file does not exist: $3"
    exit 1
fi

wav=$1
txt=$2
dict=$3
tg=$4
rec=$(echo $wav | sed 's/\.wav/\.rec/g')
lat=$(echo $txt | sed 's/\.txt/\.lat/g')  # just to remove it later
lab=$(echo $txt | sed 's/\.txt/\.lab/g')

cd $plugin_dir

echo "$(cat $txt | sed 's/ /\n/g')" > $lab

## cria lista de palavras
## NOTE isso só é necessário se o dicionário não for passado como argumento pra
## esse script, aí precisa criar o dicionário on the fly interpretando o
## binário C# com o mono no Linux
#wlist=$(mktemp --suffix='_wlist')
#while read line ; do
#    for word in $(echo $line) ; do
#        echo $word >> $wlist.tmp
#    done
#done < $txt
#sort $wlist.tmp | uniq > $wlist
#rm -f $wlist.tmp

tmp_dict=$(mktemp --suffix='_dict')

## criando o dicionário usando o g2p
#mono g2p/lapsg2p.exe -w $wlist -d $tmp_dict

# Adicionando sil no dicionário
echo -e '<s> sil\n</s> sil\n<unk> sil' > $tmp_dict
cat $dict >> $tmp_dict
echo "sil sil" >> $tmp_dict

# Rodando o HVite para gerar o arquivo alinhado
HVite -A \
    -t 1 \
    -b sil \
    -a \
    -m \
    -C confs/comp.cfg \
    -H model/hmmdefs \
    -t 250.0 \
    $tmp_dict util/phones.list $wav

success=true
python util/convert.py -r $rec -t $tg || \
    { echo "$0: there probably was a problem with HVite .rec files" >&2 && success=false; }

rm -f $lab $lat $lat.tmp $rec

cd - > /dev/null
rm -f $tmp_dict
if $success ; then
    echo "[$0] output to $tg"
else
    exit 1
fi
